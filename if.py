age = 19
if age > 19:
    print("you are young")
elif age < 40:
    print("yo")
else:
    print('other')


# python 没有 switch 替代方案
def numbers_to_strings(argument):
    switcher = {
        0: "zero",
        1: "one",
        2: "two",
    }
    return switcher.get(argument, "nothing")
