#!/usr/bin/python
# -*- coding: utf-8 -*-

def countValue(a):
    if a <= 0:
        return
    if a == 1:
        return 1
    return a * countValue(a-1)

a = int(input())

sum = 1
for i in range(1,a+1):
    sum = sum + 1 / countValue(i)

print("{:.8f}".format(sum))
# a = 1
# sum = 1
# n=int(input())
# for i in range(1, n+1):
#     a*= i
#     item=1 / a
#     sum += item
# print("{:.8f}".format(sum))