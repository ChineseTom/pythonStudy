#!/usr/bin/python
# -*- coding: utf-8 -*-

# + + 2 * 3 - 7 4 / 8 4
# 倒着数

input_list = input().split()

number_stack = []


def jishuan(a,b,fuhao):
    if fuhao == '+':
        return float(a)+float(b)
    elif fuhao == '-':
        return float(a) - float(b)
    elif fuhao == '*':
        return float(a) * float(b)
    elif fuhao == '/':
        if b == '0':
            print("ERROR")
            exit(-1)
        return float(a) / float(b)
    else:
        return 0


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False


num_count = 0
fuhao_count = 0
flag = False
for i in input_list[::-1]:
    try:
        if is_number(i):
            number_stack.append(i)
            num_count = num_count + 1
        else:
            a = number_stack.pop()
            b = number_stack.pop()
            number_stack.append(jishuan(a, b, i))
            fuhao_count = fuhao_count + 1
    except Exception:
        flag = True



# 数字 比 字符多一个
if len(number_stack) == 0 or num_count-1 != fuhao_count or flag:
    print("ERROR")
else:
    print("%0.1f" % float(number_stack.pop()))

