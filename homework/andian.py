#!/usr/bin/python
# -*- coding: utf-8 -*-


def printArr(arr):
    for x in arr:
        for tmp in x:
            print("%-4d" % tmp, end='')
        print()


n = int(input())
mylist = []
while n > 0:
    tmp = [int(x) for x in input().split()]
    mylist.append(tmp)
    n = n - 1


count = 0


n = len(mylist)
lie = []
for x in range(n):
    t = []
    for tmp in mylist:
        t.append(tmp[x])
    lie.append(t)


# printArr(mylist)
# printArr(lie)


times = 0
for x in mylist:
    for i in range(len(x)):
        if x[i] == max(x) and x[i] == min(lie[i]):
            print('%d %d' % (times, i))
            count = count + 1
    times = times + 1


if count == 0:
    print("NONE")