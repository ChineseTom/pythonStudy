#!/usr/bin/python
# -*- coding: utf-8 -*-

a = input("请输入成绩(按回车键退出):")

def getEvaluate(a):
    if a<0 or a>100 :
        return "成绩有误"
    elif  a >= 90:
        return "A"
    elif  a>=80 and a <=89:
        return "B"
    elif  a>=70 and a <=79:
        return "C"
    elif  a>=60 and a <=69:
        return "D"
    else:
        return "E"


while True:
    if not a:
        break
    score = int(a)
    s = getEvaluate(score)
    if s == "成绩有误":
        a = input("成绩有误，请重新输入成绩:")
    else:
        print(s)
        a = input("请输入成绩(按回车键退出):")


