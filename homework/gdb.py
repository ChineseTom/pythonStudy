#!/usr/bin/python
# -*- coding: utf-8 -*-
def issushu(a):
    if a <= 1:
        return False
    else:
        index = 2
        while index * index <= a:
            if a % index == 0:
                return False
            index = index + 1
        return True

n = int(input())
for i in range(1, n):
    if issushu(i) and issushu(n-i) :
        print('%d = %d + %d' % (n, i, n-i))

