#!/usr/bin/python
# -*- coding: utf-8 -*-
number = int(input())
a = input().split()

maxVal = int(a[0])
maxIndex = 0
for i in range(1, number):
    if int(a[i]) > maxVal:
        maxVal = int(a[i])
        maxIndex = i

print('%d %d' % (maxVal, maxIndex))