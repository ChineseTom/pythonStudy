#!/usr/bin/python
# -*- coding: utf-8 -*-
n = int(input().strip())
mystr = ''
mylen = 0
while n > 0:
    str = input().strip()
    if len(str) > mylen:
        mystr = str
        mylen = len(str)
    n = n - 1

print('The longest is: %s' % mystr)