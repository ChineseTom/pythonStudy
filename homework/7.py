#!/usr/bin/python
# -*- coding: utf-8 -*-

s=input("请输入密码:")

mylist = ['(', '~', '@', '#', '$', '%', '^', '&', '*', ')']

mydict = {
    '数字': 0,
    '小写': 0,
    '大写': 0,
    '特殊字符': 0
}

def getTypeLen(s):
    for i in s:
        if i >= '0' and i <= '9':
            mydict['数字'] = mydict['数字'] + 1
        elif i >= 'a' and i <= 'z':
            mydict['小写'] = mydict['小写'] + 1
        elif i >= 'A' and i <= 'Z':
            mydict['大写'] = mydict['大写'] + 1
        elif i in mylist:
            mydict['特殊字符'] = mydict['特殊字符'] + 1

    mylen=0
    for v in mydict.values():
        if v != 0:
            mylen = mylen + 1
    return mylen


if len(s) < 8:
    print("密码长度不符合要求")
else:
    len = getTypeLen(s)
    if len == 1:
        print("密码强度为一低，建议修改")
    elif len == 2:
        print("密码强度为一中，建议加强")
    elif len == 3:
        print("密码强度为一强")
    elif len == 4:
        print("密码强度为一超强")