#!/usr/bin/python
# -*- coding: utf-8 -*-


def countNumber(a):
    count = 0
    sumValue = 0
    while a != 0:
        sumValue = sumValue + a % 10
        count = count + 1
        a = int(a / 10)
    return count, sumValue


value = int(input().strip())

print('%d %d' % countNumber(value))


a, b = [int(x) for x in input().split()]

sum = 0
for i in range(a, b+1):
    sum = sum + 1/i + i * i

print("%0.6f" % sum)