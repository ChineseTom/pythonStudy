#!/usr/bin/python
# -*- coding: utf-8 -*-

def isLowerChar(c):
    return  c >= 'a' and c <= 'z'

def isUpperChar(c):
    return c >= 'A' and c <= 'Z'

mystr = input().strip()

s = ''
for i in mystr:
    if isLowerChar(i):
        s = s + chr(ord(i) - 32 )
    elif isUpperChar(i):
        s = s + chr(ord(i) + 32)
    else:
        s = s + i

print(s[:len(s)-1])