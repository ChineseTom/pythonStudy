#!/usr/bin/python
# -*- coding: utf-8 -*-

def fn(a, b):
    sum = 0
    i = 0
    tmp = 0
    while i < b:
        tmp = tmp * 10 + a
        sum = sum + tmp
        i = i + 1
    return sum


def func(a, b, c):
    return b*b-4*a*c


def sumValue(a, b):
    sum = 0
    count = 0
    for i in  range(a, b+1):
        sum = sum + i
        print('%5d' % i, end='')
        count = count + 1
        if count % 5 == 0:
            print()
    if count % 5 != 0:
        print()
    return sum


a, b= input().split()
sum = sumValue(int(a), int(b))
print('Sum = %d' % sum)
