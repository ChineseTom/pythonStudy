#!/usr/bin/python
# -*- coding: utf-8 -*-
def countValue(a):
    if a <= 0:
        return
    if a == 1:
        return 1
    return a * countValue(a-1)

a = int(input())

sum = 0
i = 1
while (2 * i - 1) <= a:
    sum = sum + countValue((2 * i - 1))
    i = i + 1


print('n=%d,s=%d' % (a,sum))