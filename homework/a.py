#!/usr/bin/python
# -*- coding: utf-8 -*-


def countValue(a, b):
    count = 1
    for i in range(b):
        count = count * a
    return count


def reverse(a, b):
    sum = 0
    times = 0
    while a != 0:
        s = a % 10
        a = int(a / 10)
        sum = sum + s * countValue(b, times)
        times = times + 1

    return sum


a,b = input().split(',')
a = int(a)
b = int(b)
print(reverse(a, b))

