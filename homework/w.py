#!/usr/bin/python
# -*- coding: utf-8 -*-



# def jishuan(x):
#     return 5 * (x - 32) / 9
#
# a, b = [int(x) for x in input().split()]
# if a > b or b > 100:
#     print("Invalid.")
# else:
#     print("fahr celsius")
#     for i in range(a, b+1, 2):
#         print('%d %6.1f' %(i,jishuan(i)))

def Fibonacci(n):
    if n < 1:
        return -1
    if n == 1 or n == 2:
        return 1
    else:
        one = 1
        two = 1
        val = 0
        for i in range(2,n):
            val = one + two
            one = two
            two = val
        return val

def fib(n):
    if n < 1:
        return -1
    if n == 1 or n == 2:
        return 1
    else:
        return fib(n-1) + fib(n-2)


def PrintFN(m, n):
    mylist = []
    for i in range(n):
        val = fib(i)
        if m <= val <= n:
            mylist.append(val)
        if val > n:
            break
    return mylist



print(Fibonacci(7))
n = int(input())
# print(Fibonacci(n))
# print(fib(n))

if n < 1:
    print("Invalid.")
else:
    count = 1
    for i in range(1, n+1):
        print("%11d" % Fibonacci(i), end='')
        if count % 5 == 0:
            print()
        count = count + 1
