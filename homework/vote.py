#!/usr/bin/python
# -*- coding: utf-8 -*-

mylist = ['6', '7', '8', '9', '10']
votes = input().split(',')
myset = (x for x in votes)

for x in myset:
    if x in mylist:
        mylist.remove(x)


mylen = len(mylist)
for i in range(mylen):
    print(mylist[i], end='')
    if i != mylen - 1:
        print(' ', end='')
