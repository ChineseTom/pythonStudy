#!/usr/bin/python
# -*- coding: utf-8 -*-


# n = int(input())
#
# for i in range(1,  n+1):
#     for j in range(1, i+1):
#         print('%d*%d=%-4d' % (j, i, i*j), end='')
#     print()


# n = int(input())
# mylist = [x for x in range(1, n+1)]
# # 1 2 3
# # 1 3 2
# # 2 1 3
# # 2 3 1
# # 3 1 2
# # 3 2 1
# for x in mylist:
#     print(x)

mylist = input().split()

count = 0
mylen = len(mylist)

for i in range(3):
    for x in range(3):
        print('%4s' % mylist[i+3*x],end='')
    if count % 3 == 0:
        print()