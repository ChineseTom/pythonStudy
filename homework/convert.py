#!/usr/bin/python
# -*- coding: utf-8 -*-


# 比较优先级
def compare(c1,c2):
    if c1 == '*' or c1 == '/' and (c2 != '*' or c2 != '/') :
        return True
    else:
        return False

def is_digit(c):
    return  c >= '0'  and c <= '9'


def check(c):
    return c in ['+','-','*','/']

input_str = input().strip()

result_stack = []

tmp_stack = []

for i in input_str:

    # 如果是数字 直接入栈
    if is_digit(i):
        result_stack.append(i)
    else:
        if len(tmp_stack) == 0:
            tmp_stack.append(i)
        elif i == ')':
            # 遇到右括号 弹出 压入到
            t = ''
            while  len(tmp_stack) != 0 and t != '(':
                t = tmp_stack.pop()
                # print("tmp is %c" % t)
                if t != '(':
                    result_stack.append(t)
        else:
             t = tmp_stack[len(tmp_stack) - 1]
             # 都是操作符号
             if check(t) and check(i):
                 if compare(i,t):
                     tmp_stack.append(i)
                     # result_stack.append(i)
                     #   如果待压栈的操作符比栈顶操作符优先级高，
                     #   则直接压栈，否则将S1中的栈顶元素出栈，并压入S2中
                 else:
                     tmp = tmp_stack.pop()
                     print(tmp)
                     result_stack.append(tmp)
                     result_stack.append(i)
             else:
                tmp_stack.append(i)

while len(tmp_stack) != 0:
    result_stack.append(tmp_stack.pop())



m_len = len(result_stack)
# 2 3 7 4 - * + 8 4 / +
for i in range(m_len):
    if i != m_len-1:
        print(result_stack[i],end=' ')
    else:
        print(result_stack[i],end='')

