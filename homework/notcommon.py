#!/usr/bin/python
# -*- coding: utf-8 -*-

a = input().split()
alen = a[0]
a = a[1::]
b = input().split()
blen = b[0]
b = b[1::]

notlist = []

mylist1 = [int(x) for x in a]
mylist2 = [int(x) for x in b]
for i in mylist1:
    if i not in mylist2:
        if i not in notlist:
            notlist.append(i)

for i in mylist2:
    if i not in mylist1:
        if i not in notlist :
            notlist.append(i)

setlen  = len(notlist)

for i in range(setlen):
    print(notlist[i],end='')
    if i != setlen-1:
        print(end=' ')

