#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import os

def write_data_txt(file_name, value):
    if not os.path.isfile(file_name):  # 无文件时创建
        fd = open(file_name, mode="w", encoding="utf-8")
        fd.close()
    index = len(value)
    with open(file_name,'w', encoding='utf-8') as file_obj:
        file_obj.write("第几把  下注金额  总成本   盈利  纯利润\n")
        count = 1
        for i in range(0, index):
            mystr = "第"+str(count)+"把\t"
            file_obj.write(mystr)
            for j in range(0, len(value[i])):
                mystr =  str(value[i][j])+"\t"
                file_obj.write(mystr)
            count = count + 1
            file_obj.write("\n")


# 首次下注金额
first = 6
# 赔率
peilu = 9.9

# 一共下注多少把
times = 50
# 利润
liren = 50

#总成本
totalSum = 0

result = list()

i = 1
while i <= times:
    totalSum += first
    if first * peilu - totalSum <= liren and i != 1:
        # 增加下注筹码
        while first * peilu <= liren + totalSum:
            first = first+1
            totalSum = totalSum + 1
        tmp = list()
        tmp.append(first)
        tmp.append(totalSum)
        tmp.append(first * peilu)
        tmp.append(first * peilu - totalSum)
        result.append(tmp)
        print(f"第{i}把 下注金额:{first} 总成本{totalSum} 盈利:{first * peilu} 纯利润{first * peilu - totalSum}")
        # result.append(tmp)
    else:
        print(f"第{i}把 下注金额:{first} 总成本{totalSum} 盈利:{first * peilu} 纯利润{first * peilu - totalSum}")
        tmp = list()
        tmp.append(first)
        tmp.append(totalSum)
        tmp.append(first * peilu)
        tmp.append(first * peilu - totalSum)
        result.append(tmp)
        first = math.ceil((liren + totalSum) / peilu)
    i = i + 1


write_data_txt("result.txt",result)


