#!/usr/bin/python
# -*- coding: utf-8 -*-

def reverse(a):
    sum = 0
    while a != 0:
        s = a % 10
        a = int(a / 10)
        sum = s + sum * 10

    return sum


n = int(input())
print(reverse(n))

