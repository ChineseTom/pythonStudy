#!/usr/bin/python
# -*- coding: utf-8 -*

import math

def issuanjiaox(a, b, c):
    return a + b > c and a + c > b and b + c > a


def countperimeter(a, b, c):
    return a + b + c


def countarea(a, b, c):
    s = (a + b + c) / 2
    return math.sqrt(s * (s-a) * (s-b) * (s-c))


a, b, c = [int(x) for x in input().split()]

if not issuanjiaox(a, b, c):
    print("These sides do not correspond to a valid triangle")
else:
    print("area = %0.2f; perimeter = %0.2f" % (countarea(a, b, c), countperimeter(a, b, c)))