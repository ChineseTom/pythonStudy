#!/usr/bin/python
# -*- coding: utf-8 -*-

n = int(input())
if n <= 0:
    print('average = 0.0')
    print('count = 0')
    exit(0)
score = input().split()

scores = [int(x) for x in score]

sum = 0
count = 0
for x in scores:
    if x >= 60:
        count = count + 1
    sum = sum + x

print('average = %0.1f' % (sum / n))
print('count = %d' % count)