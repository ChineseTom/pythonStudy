#!/usr/bin/python
# -*- coding: utf-8 -*-

# 8 3
# 8 12 7 3 20 9 5 18

count,k = [int(x) for x in input().split()]

temp = [int(x) for x in input().split()]

temp.sort()

tmpLen = len(temp)

if k > tmpLen:
    k = tmpLen

for i in range(k):
    print(temp[count - i - 1],end='')
    if i != k-1:
        print(" ",end='')


