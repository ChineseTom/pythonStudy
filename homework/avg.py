#!/usr/bin/python
# -*- coding: utf-8 -*-
a = input().split()

def getavg(mylist):
    sum = 0
    mylen = len(mylist)
    for i in range(mylen):
        sum = sum + int(mylist[i])
    return sum / mylen


avg = getavg(a)
mylen = len(a)

for i in range(mylen):
    if int(a[i]) > avg:
        print(a[i], end=' ')
