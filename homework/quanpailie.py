#!/usr/bin/python
# -*- coding: utf-8 -*-
res = []

def permute(nums):
    trace = []
    backtrack(nums,trace)
    return res

def backtrack(nums,trace):
    if len(nums) == len(trace):
        # tmpList = trace[:]
        # res.append(tmpList)
        res.append(trace)
        print(res)
        return
    for i in nums:
        if i in trace:
            continue
        trace.append(i)
        backtrack(nums,trace)
        #进入下一个选择
        trace.remove(trace[-1])



tmpList = [1,2]

def change():
    tmpList.append(3)
    return tmpList

if __name__ == "__main__":
    a = [1,2,3,4]
    print(permute(a))

