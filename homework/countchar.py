#!/usr/bin/python
# -*- coding: utf-8 -*-
#
n = int(input())

total = 1
for i in range(0, n+1):
    print('pow(3,%d) = %d' % (i, total))
    total = total * 3
