#!/usr/bin/python
# -*- coding: utf-8 -*-

def issushu(a):
    if a <= 1:
        return False
    else:
        index = 2
        while index * index <= a:
            if a % index == 0:
                return False
            index = index + 1
        return True


a, b = [int(x) for x in input().split()]

count = 0
mysum = 0
for i in range(a, b+1):
    if issushu(i):
        count = count + 1
        mysum = mysum + i

print('%d %d' % (count, mysum))


def PrimeSum(m, n):
    mysum = 0
    for i in range(a, b + 1):
        if issushu(i):
            mysum = mysum + i
    return mysum
