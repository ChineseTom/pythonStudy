count = 0

while count < 3:
    count += 1
    print(count)
else:
    print('end')


for i in range(1, 10):
    print(i)


fruit = ['apple', 'pear', 'banana']
for f in fruit:
    print(f)

a = [['apple', 'banana'], (1, 2, 5)]
for i in a:
    for x in i:
        # 一行打印
        print(x, end=' ')
else:
    print('fruit is done')


a = [1, 2, 3]
for x in a:
    if x == 2:
        break
    print(x)

for x in a:
    if x == 2:
        continue
    print(x)

