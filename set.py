myset = {1,2,3,4,5}
print(type(myset))
# set 无序 无法访问0  set 元素不重复
# print(myset[0])

set3 = {1,1,2,3}
print(len(set3))
print(1 not in set3)

set1 = {1,2,3,4}
set2 = {2,3,5}
# 求集合的差集
print(set1 - set2)
# 求集合的交集
print(set1 & set2)
# 求集合的并集
print(set1 | set2)

print(len(set()))
print(type({}))
print(type(set()))
