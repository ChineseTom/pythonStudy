
import time


# 装饰器 AOP编程思想 *args kwargs  key word 关键字参数
def decorator(func):
    def wrapper(*args, **kwargs):
        print(time.time())
        func(*args, **kwargs)
    return wrapper


@decorator
def f1():
    print('This is f1 function')


# f = decorator(f1)
# f()
f1()


@decorator
def f2(func_name, **kwargs):
    print("This is " + func_name + " function")
    print(kwargs)


f2("test", a=1, b=2)
