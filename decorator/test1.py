import time


def f1():
    print('This is f1 function')


def f2():
    print("This is f2 function")


def print_current_time(func):
    print(time.time())
    func()
    

print_current_time(f1)
