t = (1, 2, 3, True)

print(t[0])

print(t[-1:])

print((1, 2, 3) + (3, 4, 5))

print(type((1)))
print(type((1,)))
print(type(("string")))

print(ord('d'))

print(max("hello,world"))
print(min("hello,world"))