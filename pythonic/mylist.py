# 列表推导式 元组 和集合都可以推导

my_list = [1, 2, 3, 4, 5, 6, 7, 8]

b = [i*i for i in my_list if i >= 5]

print(b)

my_set = {1, 2, 3, 4, 5, 6, 7, 8}
b = {i*i for i in my_list if i >= 5}
print(b)

my_dict = {
    "wangtao": 18,
    "tom": 28
}
m_dict = {value: key for key, value in my_dict.items()}
print(m_dict)
