# python中没有switch 语法 用 字典代替

day = 6


def get_sunday():
    return "Sunday"


def get_monday():
    return "Monday"


def get_tuesday():
    return "Tuesday"


def get_default():
    return "Unknow"


switcher = {
    0: get_sunday,
    1: get_monday,
    2: get_tuesday
}



# 模拟switch语法
# day_name = switcher[day]
day_name = switcher.get(day, get_default)()

print(day_name)
