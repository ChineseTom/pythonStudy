
class Test:

   # 只能返回整形数字 True和 False 调用len() 和 bool()会触发__len__函数
    def __len__(self):
        return 0

    def __bool__(self):
        return True



test = Test()

# 对象不一定是True
if test:
    print("S")


print(bool(None))
print(bool([]))
print(bool(test))
