
def add(x, y):
    return x + y


f = lambda x, y: x + y

print(f(1, 2))

# 三元表达式

x = 2
y = 3
# 条件为真  返回结果 if 条件  else 条件为假 返回结果
max  = x if x > y else y
print(max)