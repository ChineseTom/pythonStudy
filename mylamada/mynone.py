# None python 中None 空就是空 不是空字符串
a = ''
b = []
c = False

print(None == a)
print(None == b)
print(None == c)
print(a is None)
# 判断空


def fun():
    return None

# if a  或者 if not a 用于判断空操作
# None False 类型不同 本质类型不同