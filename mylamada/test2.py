# map()

list_x =[1, 2, 3, 4, 5, 6, 7, 8]


def square(x):
    return x * x


r = map(square, list_x)
print(list(r))

r = map(lambda x: x ** 3, list_x)
print(list(r))


list_1 = [1, 2, 3, 4]
list_2 =[2, 4, 5, 6, 8]
result = map(lambda x, y : x+y, list_1, list_2)
print(list(result))