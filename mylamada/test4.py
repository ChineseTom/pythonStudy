
# filter 过滤 返回值 必须是 true或者false
list_x =[1, 2, 3, 4, 5, 6, 7, 8]


def is_odd(x):
    return x % 2 == 0


print(is_odd(2))

list_1 = filter(is_odd, list_x)

print(list(list_x))

list_2 = filter(lambda x : x%2==0, list_x)
print(list(list_2))
