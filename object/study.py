

class Student:

    name = ''
    age = 0
    sum = 0

    #  __ 为私有变量 和 私有方法

    # 构造函数 实例化对象时会自动调用 只能放回None
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print(self.__class__.sum)
        # self.__class__.sum += 1
        Student.sum += 1

    def print_info(self):
        print('name:' + self.name, 'age:' + str(self.age))

    def get_number_student(self):
        return '当前学生总数:'+str(self.sum)

    # 装饰器 类方方法 名字最好叫cls

    @classmethod
    def plus_sum(cls):
        # 操作类变量 不能使用实例变量
        cls.sum += 1
        print(cls.sum)

    @staticmethod
    def add(x, y):
        print('this is a static method')
        print(Student.sum)
        print(x, y)


name = 'tom'
age = 10
s = Student(name, age)
# s.print_info()
#
# print(type(s.__init__(name, age)))

# print(Student.name) 类变量
print(s.age)

print(s.__dict__)
print(Student.age)

print(Student.sum)

Student.plus_sum()
# s.plus_sum() 不建议

# static 方法
s.add(1, 2)
Student.add(1, 2)
