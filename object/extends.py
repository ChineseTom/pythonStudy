class Human:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def print_name(self):
        print(self.name)

    def do_homework(self):
        print('This is a parent method')


# 继承
class Teacher(Human):

    def __init__(self, name, age, score):
        # Human.__init__(self, name, age)
        # super调用父类
        super(Teacher, self).__init__(name, age)
        self.score = score

    def making(self, score):
        if score < 0:
            self.score = 0
        self.score = score
        print("本次打分成绩:"+self.score)

    def __print(self):
        print(self.score)

    def do_homework(self):
        # 调用父类 do_homework方法
        super().do_homework()
        print('This is a son method')


t = Teacher('wangtao', 18, 80)

t.print_name()

t.do_homework()


# 私有方法无法调用改函数
# t.__print()



