def damage(skill1, skill2):
    damage1 = skill1 * 2
    damage2 = skill2 * 3 + 10
    return damage1, damage2


damages = damage(10, 20)

skill1_damage, skill2_damage = damage(10, 20)
print(skill1_damage, skill2_damage)
# tuple 元组类型
print(type(damages))
# 序列解包
d = 1, 2, 3
print(type(d))
a, b, c = d
print(a, b, c)
