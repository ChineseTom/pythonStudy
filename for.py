#  range 0 开始值 10 元素个数 2 步长
for x in range(0, 10, 2):
    print(x, end=' | ')

print()

a = [1, 2, 3, 4, 5, 6, 7, 8]

for x in range(0, len(a), 2):
    print(a[x])

b = a[0:len(a):2]
print(b)
