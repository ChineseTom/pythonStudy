# 函数参数


def my_add(a, b=10):
    return a + b


print(my_add(1))

print(my_add(a=10, b=30))


def total(a=5, *numbers):
     print('a', a)
     # 遍历元组中的所有项目
     for single_item in numbers:
         print('single_item', single_item)


total(1)


# kw关键字参数 kw是一个字典

def person(name, age, **kw):
    if 'city' in kw:
        # 有city参数
        pass
    if 'job' in kw:
        # 有job参数
        pass
    print('name:', name, 'age:', age, 'other:', kw)



person('Jack', 24, city='Beijing', addr='Chaoyang', zipcode=123456)


