# from advanced.myenum import Color

from enum import Enum,unique

from enum import IntEnum

# IntEnum 只能是int类型  Enum唯一int unique 装饰器保证枚举的唯一
# Enum 类型 不能实例化 设计模式


@unique
class Color(Enum):
    RED = 1
    # red的别名
    # RED_ALIAS = 1
    YELLOW = 2
    GREEN = 3
    BLACK = 4


for c in Color.__members__.items():
    print(c)

# 枚举类型转换 将数字转换为枚举类

a = 1
print(Color(1))





