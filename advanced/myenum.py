# python枚举类型 python枚举 必须继承 Enum

from enum import Enum


class Color(Enum):
    RED = 1
    RED_ALIAS = 1
    YELLOW = 2
    GREEN = 3
    BLACK = 4


# Color 类
print(Color.RED)
# 字符串类型
print(Color.RED.name)

print(Color.RED.value)

print(Color['RED'])

# 枚举类无法更改
# Color.RED=2

# 枚举类的遍历

for c in Color:
    print(c)

color = {"red": 1, "yellow": 2, "green": 3 ,"black": 4}

class Common:
    YELLOW = 1

