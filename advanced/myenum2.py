from enum import Enum

class Color(Enum):
    RED = 1
    YELLOW = 2
    GREEN = 3
    BLACK = 4

# 枚举的比较运输
result = Color.RED == Color.GREEN
print(result)

# Color.RED 是一个枚举类型
result = Color.RED == 1
print(result)

result = Color.RED.value == 1
print(result)

result = Color.RED is Color.RED
print(result)