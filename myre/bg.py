
# 闭包 = 环境变量 a + 函数 cur


def cur_pre():
    a = 10

    def cur(x):
        print("cur function")
        return a * x * x

    # 将函数放回
    return cur


# a = 1
f = cur_pre()
print(f.__closure__)
# 获取环境变量
print(f.__closure__[0].cell_contents)
print(f(2))
