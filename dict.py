person = {
    'name': 'wangtao',
    'age': 21
}

print(person['name'])
person['age'] = 22
print(person)

# 字典 key 必须是不可变的  list   是可变的 字符串元组是不可变的
mydict = {
    1: 'tom',
    '1': {'a', 'b', 'c'},
    (1, 2): ['rose']

}

print(mydict[1])
print(mydict['1'])
print(mydict[(1, 2)])
