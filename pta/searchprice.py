#!/usr/bin/python
# -*- coding: utf-8 -*-
print("[1] apple")
print("[2] pear")
print("[3] orange")
print("[4] grape")
print("[0] exit")
myprice = {
    1: 3.00,
    2: 2.50,
    3: 4.10,
    4: 10.20
}

mylist = [int(x) for x in input().strip().split()]

count = 0
for i in mylist:
    if i == 0 or count == 5:
        break
    count += 1
    if i in myprice.keys():
        print("price = %0.2f" % myprice[i])
    else:
        print("price = 0.00")