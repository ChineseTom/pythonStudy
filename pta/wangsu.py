#!/usr/bin/python
# -*- coding: utf-8 -*-

import math


def getyinzi(n):
    mylist =[1]
    for i in range(2, int(math.sqrt(n)+1)):
        if n % i == 0:
            mylist.append(i)
            if i * i != n:
                mylist.append(n // i)

    mylist.sort()
    return mylist


a, b = [int(x) for x in input().split()]


count = 0
for i in range(a, b+1):
    tmp = getyinzi(i)
    if sum(tmp) == i:
        mystr = ''
        mylen = len(tmp)
        for j in range(mylen):
            mystr = mystr + str(tmp[j])
            if j != mylen-1:
                mystr = mystr + " + "

        print(str(i)+" = "+mystr)
        count = count + 1

if count == 0:
    print("None")
