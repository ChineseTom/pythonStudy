#!/usr/bin/python
# -*- coding: utf-8 -*-

import math

n = float(input())
a = 1
i = 2
e1 = 1
e2 = 2
while e2 - e1 >= n:
    e1 = e2
    a *= i
    e2 += 1 / a
    i = i + 1
print('{:.6f}'.format(e2))