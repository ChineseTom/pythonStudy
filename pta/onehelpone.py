#!/usr/bin/python
# -*- coding: utf-8 -*-
n = int(input())

students = []
while n > 0:
    stu = input().strip().split()
    students.append(stu)
    n -= 1


results = []
while len(students) != 0:
    res = []
    t = students[0]
    c = t[0]
    res.append(t[1])
    students.remove(t)
    for i in students[::-1]:
        if c == '0' and i[0] == '1':
            res.append(i[1])
            students.remove(i)
            break
        elif c == '1' and i[0] == '0':
            res.append(i[1])
            students.remove(i)
            break
    results.append(res)

for res in results:
    print("%s %s" % (res[0], res[1]))



