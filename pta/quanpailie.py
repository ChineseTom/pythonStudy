#!/usr/bin/python
# -*- coding: utf-8 -*-

n = int(input().strip())

mylist = [x for x in range(1,n+1)]

print(mylist)

for i in mylist:
    print(i)
