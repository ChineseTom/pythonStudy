#!/usr/bin/python
# -*- coding: utf-8 -*-


def printArr(mylist):
    for i in mylist:
        for tmp in i:
            print("%d\t" % tmp, end='')
        print()


def isxiao(mylist):
    mylen = len(mylist)
    for i in range(mylen):
        for j in range(i):
            if mylist[i][j] != 0:
                return False
    return True


n = int(input())
while n > 0:
    lenth = int(input())
    arr =[]
    for i in range(lenth):
        tmp = [int(x) for x in input().split()]
        arr.append(tmp)
    if isxiao(arr):
        print("YES")
    else:
        print("NO")
    n = n - 1