#!/usr/bin/python
# -*- coding: utf-8 -*-
mystr = input().strip()
myset = set()
for i in mystr:
    myset.add(i)

mylen = len(myset)
count = 0
for i in sorted(myset):
    print(i, end='')
    count = count + 1
    if count == mylen:
        print()

