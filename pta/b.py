#!/usr/bin/python
# -*- coding: utf-8 -*-


def issushu(a):
    if a <= 1:
        return False
    else:
        index = 2
        while index * index <= a:
            if a % index == 0:
                return False
            index = index + 1
        return True


n = int(input())

while n > 0:
    tmp = int(input())
    if issushu(tmp):
        print("Yes")
    else:
        print("No")
    n = n - 1
