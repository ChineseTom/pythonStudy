#!/usr/bin/python
# -*- coding: utf-8 -*-

# import math
# def CountDigit(number,digit):
#     number = math.fabs(number)
#     count = 0
#     while number != 0:
#         if number % 10 == digit:
#             count = count + 1
#         number = number // 10
#     return count
#
#
# number,digit=input().split()
# number=int(number)
# digit=int(digit)
# count=CountDigit(number,digit )
# print("Number of digit 2 in "+str(number)+":",count)

import math


def countNum(n):
    count = 1
    for i in range(n-1):
        count = count * 10
    return count


def CountDigit(number,n):
    mysum = 0
    tmp = number
    while number != 0:
        mysum = mysum + math.pow(number % 10 , n)
        number = number // 10
    # print('mysum=%d tmp=%d' % (mysum,tmp))
    return mysum == tmp


n = int(input())

for i in range(countNum(n), countNum(n+1)):
    if CountDigit(i, n):
        print(i)
# print(CountDigit(123))