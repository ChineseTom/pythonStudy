#!/usr/bin/python
# -*- coding: utf-8 -*-
n = int(input())

scorehigh = []
maxVal = 0
while n > 0:
    student = input().split()
    tmp = sum([int(x) for x in student[2::]])
    if tmp > maxVal:
        maxVal = tmp
        scorehigh = []
        scorehigh.append(student[1])
        scorehigh.append(student[0])
        scorehigh.append(maxVal)

    n = n - 1

mylen = len(scorehigh)
for i in range(mylen):
    print(scorehigh[i], end='')
    if i != mylen - 1:
        print(' ', end='')
