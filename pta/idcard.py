#!/usr/bin/python
# -*- coding: utf-8 -*-
mylist = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]

mydict = {
    0: '1',
    1: '0',
    2: 'X',
    3: '9',
    4: '8',
    5: '7',
    6: '6',
    7: '5',
    8: '4',
    9: '3',
    10: '2'
}


def isNumber(c):
    return '0' <= c <= '9'


def validate(idcard):
    mylen = len(idcard)
    if mylen != 18:
        return False
    else:
        count = 0
        for i in range(mylen - 1):
            if not isNumber(idcard[i]):
                return False
            else:
                count = count + int(idcard[i]) * mylist[i]

        count = count % 11
        c = mydict[count]
        if str(c) != idcard[mylen - 1]:
            return False
        else:
            return True


n = int(input())
count = 0
for i in range(n):
    idcard = input().strip()
    if not validate(idcard):
        print(idcard)
        count = count + 1

if count == 0:
    print("All passed")
