#!/usr/bin/python
# -*- coding: utf-8 -*-
mylist = [int(x) for x in input().split()]

result = []
i = 0
if mylist[i] == 0:
    print("0 0", end='')
else:
    mylen = len(mylist)
    for i in range(0, mylen, 2):
        if mylist[i] == 0:
            continue
        if abs(mylist[i]) <= 1000 and abs((mylist[i+1])):
            if mylist[i] * mylist[i + 1] != 0:
                result.append(mylist[i]*mylist[i+1])
                result.append(mylist[i+1]-1)

    if len(result) != 0:
        for i in range(len(result)):
            print(result[i], end='')
            if i != len(result) - 1:
                print(end=" ")
    else:
        print("0 0")
