#!/usr/bin/python
# -*- coding: utf-8 -*-
s = input().strip()
d = input().strip()

def convert(c):
    if 'a' <= c <= 'z':
        return chr(ord(c) - 32)
    if 'A' <= c <= 'Z':
        return chr(ord(c) + 32)
    return c


newstr = ""

for t in s:
    if t not in d and convert(t) not in d:
        newstr = newstr + t

print("result: %s" % newstr)
