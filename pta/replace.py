#!/usr/bin/python
# -*- coding: utf-8 -*-
s = input()
mydict = {
}

for x in range(26):
    mydict[chr(ord('A') + x)] = chr(ord('Z') - x)


mystr =''
for x in s:
    if x in mydict.keys():
        mystr = mystr + mydict[x]
    else:
        mystr = mystr + x

print(mystr)