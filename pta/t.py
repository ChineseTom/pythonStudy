#!/usr/bin/python
# -*- coding: utf-8 -*-
mylist = [int(x) for x in input().split()]

result = []
n = 0
mylen = len(mylist)
t = []
for i in range(1, mylen+1):
    t.append(mylist[i-1])
    if i % 3 == 0:
        result.append(t)
        t = []


for tmp in result:
    for x in tmp:
        print("%4d" % x, end='')
    print("%4d%4d" % (max(tmp), sum(tmp)))
