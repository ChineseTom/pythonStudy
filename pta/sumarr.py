#!/usr/bin/python
# -*- coding: utf-8 -*-


# 4
# 2 3 4 1
# 5 6 1 1
# 7 1 8 1
# 1 1 1 1
n = int(input())
sum = 0
for i in range(n):
    mylist = [int(x) for x in input().split()]
    mylen = len(mylist)
    if i != n - 1:
        for j in range(mylen-1):
            if j != n - i - 1:
                print('mylist[%d]= %d' % (j,mylist[j]),end=' ')
                sum = sum + mylist[j]
        print()

print('sum = %d' % sum)
