#!/usr/bin/python
# -*- coding: utf-8 -*-


# l=list(map(int,input().split(',')))
# num=int(input())
# d={}
# for i in l:
#     d[i]=num-i
# for key,value in d.items():
#     if key in l and value in l:
#         print(l.index(key),l.index(value))
#         break
# else:
#     print('no answer')

mylist = [int(x) for x in input().split(',')]
target = int(input())

mylen = len(mylist)
result = []

for i in range(0, mylen):
    for j in range(i+1, mylen):
        if mylist[i] + mylist[j] == target:
            result.append(i)
            result.append(j)
            break

if len(result) >= 2:
    for i in range(len(result)):
        print(result[i],end='')
        if i != len(result)-1:
            print(" ",end='')
else:
    print("not found")