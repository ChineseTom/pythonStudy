#!/usr/bin/python
# -*- coding: utf-8 -*-

def myfunc(a,b,c):
    mydict = {
        '+': a + b,
        '-': a - b,
        '*': a * b,
        '/': a / b
    }
    return mydict[c]

a = int(input())
c = input().strip()
b = int(input())

if c == '/' and b == 0:
    print("divided by zero")
else:
    print("%0.2f" %(myfunc(a,b,c)))