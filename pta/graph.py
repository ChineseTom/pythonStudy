#!/usr/bin/python
# -*- coding: utf-8 -*-

n = int(input())
t = n
weights = []
while n > 0:
    w = eval(input().strip())
    weights.append(w)
    n -= 1
# 4
# {'a':{'b':10,'c':6,'d':10}}
# {'b':{'c':2,'d':7}}
# {'c':{'d':10}}
# {'d':{'a':10}}


totalsum = 0
total = 0
myset = set()

for weight in weights:
   for w in weight:
       for i in weight[w]:
           if str(w+"---->"+i) not in myset and str(i+"---->"+w) not in myset:
               # print(w+"---->"+i)
               myset.add(w+"---->"+i)
               total += weight[w][i]
               totalsum += 1


print("%d %d %d" % (t, totalsum, total))
