#!/usr/bin/python
# -*- coding: utf-8 -*-

a = input().split()

mylist = [int(x) for x in a]

mydict = {

}

for x in mylist:
    if x not in mydict.keys():
        mydict[x] = 1
    else:
        mydict[x] = mydict[x] + 1


maxKey= 0
maxValue = 0
for k,v in mydict.items():
    if v >= maxValue:
        maxKey,maxValue = k,v

print(maxKey,maxValue)

