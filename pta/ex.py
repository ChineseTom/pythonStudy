#!/usr/bin/python
# -*- coding: utf-8 -*-
def fib(n):
    if n < 0:
        return -1
    if n == 0 or n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)


def PrintFN(m, n):
    mylist = []
    for i in range(n):
        val = fib(i)
        if m <= val <= n:
            mylist.append(val)
        if val > n:
            break
    return mylist


m,n,i=input().split()
n=int(n)
m=int(m)
i=int(i)
b=fib(i)
print("fib({0}) = {1}".format(i,b))
fiblist=PrintFN(m,n)
print(len(fiblist))