#!/usr/bin/python
# -*- coding: utf-8 -*-

def isUpper(c):
    return c >='A' and c <= 'Z'

s =  input().strip()

mylist = []

for i in s:
    if isUpper(i):
        if i not in mylist:
            mylist.append(i)

mylen = len(mylist)

if mylen == 0:
    print('Not Found')
else:
    for i in mylist:
        print(i,end='')
