#!/usr/bin/python
# -*- coding: utf-8 -*-

def shuttleDict(i,times):
    mysum = 0
    if type(i) == list or type(i) == tuple:
        for j in i:
           mysum  += shuttleDict(j, times + 1)
    elif type(i) == type(1):
        # mysum += times
        mysum += i * times
    return mysum



a = eval(input())
layer = int(input())
def fn(base, deep, layer):
    if deep == layer:
        return len([i for i in base if type(i) == int])
    else:
        return sum(fn(i, deep+1, layer) for i in base if type(i) == list)
print(fn(a, 1, layer))

