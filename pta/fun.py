#!/usr/bin/python
# -*- coding: utf-8 -*-

def fun(a):
    if a == 0:
        return 0
    elif a != 0:
        return 1 / a

reslut = float(input().strip())

print('f(%0.1f) = %0.1f' % (reslut, fun(reslut)))



