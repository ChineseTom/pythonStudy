
# i = 0
#
# def isLeapYear(year):
#     if(year % 4 != 0):
#         return False
#     elif year % 100 == 0 and year % 4 != 0:
#         return False
#     else:
#         return True
#
# for year in range(2000,3000):
#     if isLeapYear(year):
#         print(str(year),end='\t')
#         i = i+1
#         if(i % 4 == 0):
#             print()

n = int(input())

sum = 0
oldfenmu = 0
oldfenzi = 0
for i in range(n):
    if i == 0:
        fenmu = 1
        fenzi = 2
        oldfenmu = 1
        oldfenzi = 2
    else:
        fenmu = oldfenzi
        fenzi = oldfenmu + oldfenzi
        oldfenmu = fenmu
        oldfenzi = fenzi
    sum = sum + fenzi / fenmu
print('%0.2f' % sum)