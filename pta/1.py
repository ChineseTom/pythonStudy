# #!/usr/bin/python
# # -*- coding: utf-8 -*-
#
# a=input("请输入成绩:")
# score=int(a)
#
# def getEvaluate(a):
#     if a<0 or a>100 :
#         return "成绩有误"
#     elif  a >= 90:
#         return "A"
#     elif  a>=80 and a <=89:
#         return "B"
#     elif  a>=70 and a <=79:
#         return "C"
#     elif  a>=60 and a <=69:
#         return "D"
#     else:
#         return "E"
#
#
# print(getEvaluate(score))
# days = {
#     1 : "Mon",
#     2 : "Tue",
#     3 : "Wed",
#     4 : "Thu",
#     5 : "Fri",
#     6 : "Sat",
#     7 : "Sun"
# }
# day = int(input())
# print(days[day])


def Fibonacci(n):
    if n < 1:
        return -1
    if n == 1 or n == 2:
        return 1
    else:
        one = 1
        two = 1
        val = 0
        for i in range(2,n):
            val = one + two
            one = two
            two = val
        return val

n = int(input())

count = 1
while True:
    val = Fibonacci(count)
    if val > n:
        print(val)
        break
    count = count + 1
