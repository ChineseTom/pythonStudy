#!/usr/bin/python
# -*- coding: utf-8 -*-
# [4,4]

mystr = input().replace('[', '').replace(']', '').strip()
s = list(mystr.split(','))
mylist = []

mylen = len(s)
for i in range(mylen):
    if s[i] not in mylist:
        mylist.append(s[i])

mylistlen = len(mylist)

for i in range(mylistlen):
    print(mylist[i], end='')
    if i != mylistlen-1:
        print(end=' ')
