#!/usr/bin/python
# -*- coding: utf-8 -*-
letter, blank, digit, other = 0, 0, 0, 0
s = input()

while 1:
    for i in s:
        if i.isalpha():
            letter += 1
        elif i.isdigit():
            digit += 1
        elif i.isspace():
            blank += 1
        else:
            other += 1
    if letter + digit + blank + other >= 10:
        break

    blank += 1
    s = input()

print("letter = %d, blank = %d, digit = %d, other = %d" % (letter, blank, digit, other))