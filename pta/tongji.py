#!/usr/bin/python
# -*- coding: utf-8 -*-

n = int(input())
if n <= 0:
    exit(-1)
mylist = [int(x) for x in input().split()]

mydict = {}
for i in mylist:
    if i not in mydict.keys():
        mydict[i] = 1
    else:
        mydict[i] = mydict[i] + 1



order=sorted(mydict.items(), key=lambda x:x[0], reverse=False)

for k,v in order:
    print('%d:%d' % (k,v))