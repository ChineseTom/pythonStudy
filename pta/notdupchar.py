#!/usr/bin/python
# -*- coding: utf-8 -*-


def convert(c):
    if 'a' <= c <= 'z':
        return chr(ord(c) - 32)
    if 'A' <= c <= 'Z':
        return chr(ord(c) + 32)
    return c


mystr = input().strip()

if len(mystr) < 10:
    print("not found")
else:
    result = ""
    for i in mystr:
        if i.isalpha() and i not in result and convert(i) not in result and i != ' ':
            result = result + i
            if len(result) == 10:
                break
    if len(result) == 10:
        print(result)
    else:
        print("not found")
