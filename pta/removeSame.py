#!/usr/bin/python
# -*- coding: utf-8 -*-
(a,b,c) = input().split()
my = (int(a), int(b), int(c))

mylist = list(my)
mylist.sort()
length = len(mylist)

for i in range(length):
    print(mylist[i], end='')
    if(i != length-1):
        print("->", end='')
    print()
