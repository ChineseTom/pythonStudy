#!/usr/bin/python
# -*- coding: utf-8 -*-

def convert(c):
    if 'a' <= c <= 'z':
        c = chr(ord(c) - 32)
    return c

def acronym(phrase):
    words = phrase.split()
    mystr = ''
    for word in words:
        mystr = mystr + convert(word[0])
    return mystr

print(acronym('central  processing  unit'))

