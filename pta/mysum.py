#!/usr/bin/python
# -*- coding: utf-8 -*-


#@link https://pintia.cn/problem-sets/1111652100718116864/problems/1163035698160459782
#todo has some problem to fix this bug

def mysum(mylist):
    if (len(mylist) == 0):
        return 0
    tmp = 0
    for i in mylist:
        if type(i) != list and type(i) != tuple:
            tmp += i
        else:
            return tmp + mysum(i)
    return tmp


mylist = eval(input().strip())
result = 0
for i in mylist:
    if type(i) == list or type(i) == tuple:
        result += mysum(i)
    elif type(i) == type(1):
        result += i

print(result)

# this is correct answer
def shuttle(lst):
    t=[]
    for i in lst:
        if type(i) == list or type(i) == tuple:
            for j in shuttle(i):
                t.append(j)
        elif type(i) == type(1):
            t.append(i)
    return t
lst=eval(input())
print(sum(shuttle(lst)))

def shuttleDict(lst):
    t=[]
    times = 1
    for i in lst:
        if type(i) == list or type(i) == tuple:
            for j in shuttleDict(i):
                times +=1
                t.append(times * j)
        elif type(i) == type(1):
            t.append(times*i)
    return t
