#!/usr/bin/python
# -*- coding: utf-8 -*-


def countDian(x):
    if x <= 15:
        return 4 * x / 3
    else:
        return 2.5 * x - 17.5


def isDigit(a):
    return a >= '0' and a <= '9'



s = input().strip()
# print(s[::-1])


mystr=''

for i in s:
    if isDigit(i):
        mystr = mystr + i

print(int(mystr))