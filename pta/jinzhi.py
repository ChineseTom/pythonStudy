#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
# +-P-xF4+-1!#
def ishexstr(c):
    s = '1234567890abcdefABCDEF'
    return c in s


mystr =input().strip()
mystr = mystr[:mystr.find("#")]

result = ""
sign = 1
for i in mystr:
    if ishexstr(i):
        result += i

if result == "":
    print("0")
else:
    if mystr.find('-') < mystr.find(result[0]):
        sign = -sign
    print(int(result,16) * sign)
