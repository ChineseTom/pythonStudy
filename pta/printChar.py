#!/usr/bin/python
# -*- coding: utf-8 -*-

n = int(input())

c = 'A'
num = ord(c)
count = 0
while n >= 0:
    for i in range(n):
        print("%s " % chr(num+count), end='')
        count = count + 1
    if n != 0:
        print()
    n = n - 1
