#!/usr/bin/python
# -*- coding: utf-8 -*-

n = int(input())

maxFive = n // 5

count = 0
for i in range(maxFive, 0, -1):
    # 5 从 1开始
    maxTwo = (n - i * 5) // 2
    for j in range(maxTwo, 0, -1):
        maxOne = n - i * 5 - j * 2
        if maxOne > 0:
            print("fen5:%d, fen2:%d, fen1:%d, total:%d" % (i, j, maxOne, i+j+maxOne))
            count += 1

print("count = %d" % count)
