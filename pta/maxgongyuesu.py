#!/usr/bin/python
# -*- coding: utf-8 -*-
a, b = [int(x) for x in input().split()]


def gongyuesu(a, b):
    if a < b:
        a, b = b, a
    while b != 0:
        tmp = a % b
        a = b
        b = tmp
    return a

print('%d %d' % (gongyuesu(a,b), a * b / gongyuesu(a,b)))

