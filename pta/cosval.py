#!/usr/bin/python
# -*- coding: utf-8 -*-

import math


def func(n):
    val = 1
    if n == 1 or n == 0:
        return 1
    for i in range(1,n+1):
        val = val * i
    return val


def funcos(eps, x):
    mysum = 0
    i = 0
    times = 1
    while True:
        tmp = math.pow(x, i) / func(i)
        if times % 2 == 0:
            tmp = -tmp
        if abs(tmp) < eps:
            break
        mysum = mysum + tmp
        i = i + 2
        times = times + 1
    return mysum


eps = float(input())
x = float(input())
value=funcos(eps,x)
print("cos({0}) = {1:.4f}".format(x,value))
