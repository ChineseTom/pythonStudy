#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
s = sys.stdin.read()
s = s[:s.find("#")]
t = set([i if i.isalnum() == False and i != '_' else " " for i in s])
for i in t:
    s = s.replace(i," ")
word = s.lower().split(" ")

dict ={}
for w in word:
    w = w[:15]
    # 统计字符
    dict[w] = dict.get(w, 0) + 1
del dict[""]
# 进行排序
result=sorted(dict.items(),key=lambda x:(-x[1],x[0]))
print(len(result))
all=int(0.1*len(result))
for i in range(0,all):
    print(str(result[i][1])+":"+result[i][0])
