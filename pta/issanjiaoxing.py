
#!/usr/bin/python
# -*- coding: utf-8 -*-

def issuan(a,b,c):
    return a + b > c \
           and b + c > a \
           and a + c > b

a,b,c = [int(x) for x in input().split()]

if issuan(a, b, c):
    print("yes")
else:
    print("no")