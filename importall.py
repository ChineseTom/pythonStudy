import commonimport

# 包和模块不会重复导入
# 避免循环导入  a 导入 b   b 导入 a 导致循环导入
print(commonimport.sys.path)
