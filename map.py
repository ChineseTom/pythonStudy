person = {
    "name": "wangtao",
    "age": 21,
    "odd": False
}

print(person["name"])

# 0 8 进行切片操作 hello,wo   每隔2个字符进行输出
print("hello,world"[0:8:2])

print(2 in [1, 2])

print(3 not in [1, 2])

print(len(person))
