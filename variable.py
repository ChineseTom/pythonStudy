a = [1, 2, 3]
b = [4, 5]
print(a*3 + b + a)
# int str truple 值类型  set dict list 类型
num = 1
temp = num
num = 2
print(temp)

list1 = ['a', 'b']
list2 = list1
list2[0] = 'c'
print(list1)

a = 'hello'
print(hex(id(a)))
a = a + ' world'
print(hex(id(a)))
print(a)
# 字符串是不可变的对象 无法复制
# 'helo'[0] = 'd'

number = ['1', '2']
print(hex(id(number)))
number[0] = '3'
print(hex(id(number)))

number.append('4')
print(number)
print(hex(id(number)))

