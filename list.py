students = ["jack", "tom", "rose"]
for s in students:
    print(s)

lists = [1, "st", 3, "apple", True, False]
print(type(lists))

print(lists[0:2])
print(lists[-1:])

lists[0] = 2
print(lists[0])
lists.append("ruby")
print(lists)

print([1, 2] * 3)

foot_ball_team = [["巴西", "中国"], ["日本", "美国"]]

numbers = [1, 4, 5, 10, -1]
print(max(numbers))
print(min(numbers))
