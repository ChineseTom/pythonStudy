
import sys

# 设置最大递归深度
sys.setrecursionlimit(100000000000)


def swap(a, b):
    a, b = b, a


a = 1
b = 2
swap(a, b)
print(a, b)
a, b = b, a
print(a, b)


def my_add(a, b):
    return a + b


print(my_add(1, 2))

# def print(code):
#     print(code)
#
#
# print('a')


